var express = require('express');

module.exports = function(io) {
    'use strict';
    var leagueRouter = express.Router();
    var League = require('../models/league');
    var AuthService = new (require('../../auth/services/auth'));

    // on routes that end in /league/:id
    // ----------------------------------------------------
    leagueRouter.route('/:id')

        // get the league with that id
        .get(AuthService.isLoggedIn, function(req, res) {
            console.log('getting league', req.params.id);
            League.findById(req.params.id, function(err, league) {
                if (err)
                    res.send(err);
                res.json(league);
            });
        })

        // update the league with this id
        .put(AuthService.isLoggedIn, function(req, res) {
            League.findById(req.params.id, function(err, league) {

                if (err)
                    res.send(err);

                league.name = req.body.name;
                league.save(function(err) {
                    if (err)
                        res.send(err);

                    res.json({ message: 'League updated!' });
                });

            });
        })

        // delete the league with this id
        .delete(AuthService.isLoggedIn, function(req, res) {
            League.remove({
                _id: req.params.id
            }, function(err, league) {
                if (err)
                    res.send(err);

                res.json({ message: 'Successfully deleted' });
            });
        });


    leagueRouter.route('/')
        // create a league (accessed at POST http://localhost:8080/api/league)
        .post(AuthService.isLoggedIn, function(req, res) {
            var league = new League();      // create a new instance of the league model
            league.name = req.body.name;

            league.save(function(err) {
                if (err)
                    res.send(err);
                res.json(league);
            });

        })

        // get all the leagues (accessed at GET http://localhost:8080/api/league)
        .get(AuthService.isLoggedIn, function(req, res) {
            console.log('getting all leagues');
            League.find(function(err, leagues) {
                if (err)
                    res.send(err);

                res.json(leagues);
            });
        });

    return leagueRouter;
};