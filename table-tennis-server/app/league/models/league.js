var mongoose = require('mongoose');

var LeagueSchema = new mongoose.Schema({
    name: String
});

module.exports = mongoose.model('League', LeagueSchema);
