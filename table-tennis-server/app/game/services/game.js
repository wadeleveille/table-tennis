(function() {
    "use strict";

    module.exports = function (io) {
        var methods = {};

        methods.createGameRoom = function(gameId) {
            var gameRoom = io.of('/' + gameId);
            console.log('room created: ' + gameId);
            gameRoom.on('connection', function(socket){
                console.log('someone connected');
                socket.on('user:add', function(data){
                    console.log('incoming message',data);
                    gameRoom.emit('user:added',data);
                });
                socket.on('user:timerStart', function(data){
                    console.log('incoming message',data);
                    gameRoom.emit('user:timerStarted',data);
                });
                socket.on('user:timerStop', function(data){
                    console.log('incoming message',data);
                    gameRoom.emit('user:timerStopped',data);
                });
                socket.on('user:timerReset', function(data){
                    console.log('incoming message',data);
                    gameRoom.emit('user:timerResetted',data);
                });
                socket.on('user:strokesUpdated', function(data){
                    console.log('incoming message',data);
                    gameRoom.emit('user:strokesUpdated',data);
                });
            });
        };

        return methods;
    };
}());