var express = require('express');

module.exports = function(io) {
    'use strict';
    var gameRouter = express.Router();
    var Game = require('../models/game');
    var GameUser = require('../models/game-user');
    var GameService = new (require('../services/game'))(io);
    var AuthService = new (require('../../auth/services/auth'));

    // on routes that end in /game/:id
    // ----------------------------------------------------
    gameRouter.route('/:id')

        // get the game with that id
        .get(AuthService.isLoggedIn, function(req, res) {
            console.log('getting game', req.params.id);
            Game.findById(req.params.id, function(err, game) {
                if (err)
                    res.send(err);
                res.json(game);
            });
        })

        // update the game with this id
        .put(AuthService.isLoggedIn, function(req, res) {
            Game.findById(req.params.id, function(err, game) {

                if (err)
                    res.send(err);

                game.friendlyId = req.body.friendlyId;
                game.name = req.body.name;
                game.users = req.body.users;
                game.save(function(err) {
                    if (err)
                        res.send(err);

                    res.json({ message: 'Game updated!' });
                });

            });
        })

        // delete the game with this id
        .delete(AuthService.isLoggedIn, function(req, res) {
            Game.remove({
                _id: req.params.id
            }, function(err, game) {
                if (err)
                    res.send(err);

                res.json({ message: 'Successfully deleted' });
            });
        });


    gameRouter.route('/')
        // create a game (accessed at POST http://localhost:8080/api/game)
        .post(AuthService.isLoggedIn, function(req, res) {
            var game = new Game();      // create a new instance of the game model
            game.friendlyId = (Math.random().toString(36)+'00000000000000000').slice(2, 5+2);
            game.name = req.body.name;
            game.users = req.body.users;

            game.save(function(err) {
                if (err)
                    res.send(err);
                GameService.createGameRoom(game._id);
                res.json(game);
            });

        })

        // get all the games (accessed at GET http://localhost:8080/api/game)
        .get(AuthService.isLoggedIn, function(req, res) {
            console.log('getting all games');
            Game.find(function(err, games) {
                if (err)
                    res.send(err);

                res.json(games);
            });
        });

    // on routes that end in /game/:id
    // ----------------------------------------------------
    gameRouter.route('/findByFriendlyId/:id')

        // get the game with that id
        .get(AuthService.isLoggedIn, function(req, res) {
            console.log('getting game by friendlyid', req.params.id);
            Game.find({ 'friendlyId': req.params.id }, function(err, game) {
                if (err)
                    res.send(err);
                res.json(game);
            });
        });

    return gameRouter;
};