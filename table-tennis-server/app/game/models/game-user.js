var mongoose = require('mongoose');

var GameUserSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    game: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Game'
    },
    currentTime: Number,
    currentStrokes: Number
});

module.exports = mongoose.model('GameUser', GameUserSchema);
