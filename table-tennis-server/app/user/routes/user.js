var express = require('express');

module.exports = function(io, passport) {
    'use strict';
    var userRouter = express.Router();
    var User = require('../models/user');
    var AuthService = new (require('../../auth/services/auth'));
    var passport = require('passport');

    // on routes that end in /user/:id
    // ----------------------------------------------------
    userRouter.route('/:id')

        // get the user with that id
        .get(AuthService.isLoggedIn, function(req, res) {
            console.log('getting user', req.params.id);
            User.findById(req.params.id, function(err, user) {
                if (err)
                    res.send(err);

                res.json(user);
            });
        })

        // update the user with this id
        .put(AuthService.isLoggedIn, function(req, res) {
            User.findById(req.params.id, function(err, user) {

                if (err)
                    res.send(err);

                user.firstName = req.body.firstName;
                user.lastName = req.body.lastName;
                user.username = req.body.username;
                user.save(function(err) {
                    if (err)
                        res.send(err);

                    res.json({ message: 'User updated!' });
                });

            });
        })

        // delete the user with this id
        .delete(AuthService.isLoggedIn, function(req, res) {
            User.remove({
                _id: req.params.id
            }, function(err, user) {
                if (err)
                    res.send(err);

                res.json({ message: 'Successfully deleted' });
            });
        });


    userRouter.route('/')
        // create a user (accessed at POST http://localhost:8080/api/user)
        .post(AuthService.isLoggedIn, function(req, res) {
            var user = new User();      // create a new instance of the user model
            user.firstName = req.body.firstName;
            user.lastName = req.body.lastName;
            user.username = req.body.username;
            user.password = AuthService.generateHash(req.body.password);

            user.save(function(err) {
                if (err)
                    res.send(err);

                res.json({ message: 'User created!' });
            });

        })

        // get all the users (accessed at GET http://localhost:8080/api/user)
        .get(AuthService.isLoggedIn, function(req, res) {
            console.log('getting all users');
            User.find(function(err, users) {
                if (err)
                    res.send(err);

                return res.json(users);
            });
            io.emit('event:test','blah');
        });

    return userRouter;
};
