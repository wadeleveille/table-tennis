(function() {
    "use strict";

    module.exports = function (io) {
        var User = require('../models/user');
        var methods = {};

        methods.findByUsername = function(username) {
            return User.findOne({ username: username });
        };

        return methods;
    };
}());