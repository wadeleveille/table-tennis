var express = require('express');

module.exports = function(io, passport) {
    'use strict';
    var authRouter = express.Router();
    var User = require('../../user/models/user');
    var AuthService = new (require('../../auth/services/auth'));
    var passport = require('passport');

    // on routes that end in /user/:id
    // ----------------------------------------------------
    authRouter.route('/login')

        // attempt login
        .post(function(req, res, next) {
            passport.authenticate('local-login', function(err, user, info) {
                console.log(err, user, info);
                if(err) {
                    return next(err);
                }
                if(!user) {
                    return res.json(404, info);
                }
                req.logIn(user, function (err) {
                    if (err) { return next(err); }
                    return res.json(200, user);
                });

            })(req, res, next);
        });

    authRouter.route('/logout')

        //logout
        .get(function(req, res) {
            req.logout();
            return res.json(200, {message: 'logged out'});
        });

    return authRouter;
};
