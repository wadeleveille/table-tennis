var bcrypt = require('bcrypt-nodejs');

(function() {
    "use strict";

    module.exports = function () {
        var User = require('../../user/services/user');
        var methods = {};

        methods.authenticate = function(username, password, done) {
            User.findByUsername(username, function(err, user) {
                if (err) {
                    console.log('error');
                    return done(err);
                }
                if (!user) {
                    console.log('invalid username');
                    return done(null, false, { message: 'Incorrect username.' });
                }
                if (!user.validPassword(password)) {
                    console.log('invalid password');
                    return done(null, false, { message: 'Incorrect password.' });
                }
                return done(null, user);
            });
        };

        // route middleware to make sure
        methods.isLoggedIn = function(req, res, next) {
            console.log('is logged in?', req.isAuthenticated());
            // if user is authenticated in the session, carry on
            if (req.isAuthenticated()) {
                return next();
            }

            // if they aren't respond with 401
            res.statusCode = 401;
            res.end();
        };

        methods.isValidPassword = function(enteredPassword, dbPassword) {
            return bcrypt.compareSync(enteredPassword, dbPassword);
        };

        // generating a hash
        methods.generateHash = function(password) {
            return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
        };

        return methods;
    };
}());