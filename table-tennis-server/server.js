// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express = require('express');        // call express
var app = express();                 // define our app using express
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var errorHandler = require('errorhandler');
var path = require('path');
var http = require('http');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var passportConfig = require('./app/auth/services/passport')(passport); // pass passport for configuration


// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(cookieParser());
// required for passport
app.use(session({ secret: 'tabletennis',
                 saveUninitialized: true,
                 resave: true} )); // session secret
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use(errorHandler({ dumpExceptions: true, showStack: true }));

var apiPort = process.env.PORT || 9500;        // set our apiPort
var socketPort = process.env.PORT || 9501;        // set our apiPort

// DB SETUP
var mongoose   = require('mongoose');
mongoose.connect('mongodb://localhost/tabletennis'); // connect to our database


// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    var origin = req.get('origin');

    console.log('%s %s %s %s', origin, req.method, req.url, req.path);

    // sure, we'll allow everyone
    res.header('Access-Control-Allow-Origin', origin);
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next(); // make sure we go to the next routes and don't stop here
});

// test route to make sure everything is working (accessed at GET http://localhost:9500/api)
router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });
});

// START THE SERVER
// =============================================================================
var httpServer = app.listen(apiPort);
console.log('Table Tennis running on port ' + apiPort);


var socketServer = require('http').Server(app);
var io = require('socket.io')(socketServer);

io.on('connection', function(socket){
    console.log('socket connected');
    socket.on('event:new:message',function(data){
        console.log('incoming message',data);
        io.emit('event:incoming:message',data);
    });
    socket.on('disconnect', function () {
        console.log('socket disconnected');
    });
});

socketServer.listen(socketPort,function(){
    console.log('Socket.io running on port ' + socketPort);
});


// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// more routes for our API will happen here
var authRouter = require('./app/auth/routes/auth')(io, passport);
app.use('/api/auth', authRouter);
var userRoute = require('./app/user/routes/user')(io, passport);
app.use('/api/user', userRoute);
var gameRouter = require('./app/game/routes/game')(io, passport);
app.use('/api/game', gameRouter);
var leagueRouter = require('./app/league/routes/league')(io, passport);
app.use('/api/league', leagueRouter);


// BIND CLOSE EVENTS
process.on('SIGINT', function () {
  console.log("Closing");
  httpServer.close();
  socketServer.close();
});

httpServer.on('close', function () {
  console.log("Closed");
  mongoose.disconnect();
});