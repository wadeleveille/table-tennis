angular.module('tableTennis')
    .controller('GameListCtrl', [
        '$scope',
        'Game',
        'OppSocket',
        '$state',
        function($scope, Game, OppSocket, $state) {
            "use strict";

            $scope.games = Game.all();

            $scope.game = {
                name: ''
            };

            $scope.createGame = function() {
                $scope.game = Game.create($scope.game, function() {
                    $state.go('game-detail', { "gameId": $scope.game._id });
                });
            };
        }
    ]);

