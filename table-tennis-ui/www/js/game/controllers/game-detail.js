angular.module('tableTennis')
    .controller('GameDetailCtrl', [
        '$scope',
        'User',
        'OppSocket',
        'Game',
        '$stateParams',
        function($scope, User, OppSocket, Game, $stateParams) {
            "use strict";

            $scope.game = Game.get({id: $stateParams.gameId}, function() {
                OppSocket.openGameSocket($scope.game._id);
                OppSocket.afterMessage(OppSocket.getGameSocket(),'user:added',function(data){
                    $scope.$apply(function(){
                        $scope.users.push(data);
                    });
                });
            });
            $scope.availableUsers = User.all();
            $scope.users = [];



            $scope.addUser = function(user) {
                OppSocket.sendMessage(OppSocket.getGameSocket(),'user:add',user);
            };
        }
    ]);