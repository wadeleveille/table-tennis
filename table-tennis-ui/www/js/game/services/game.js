angular.module('tableTennis')
    .service('Game', [
        '$resource',
        function ($resource) {
            "use strict";

            return $resource('http://localhost:9500/api/game/:id', {}, {
                all: {
                    method: 'GET',
                    url: 'http://localhost:9500/api/game',
                    isArray: true,
                    transformResponse: function (data) {
                        try {
                            var responseObj = JSON.parse(data);
                            return responseObj;
                        } catch (e) {
                            return {};
                        }
                    }
                },

                get: {
                    method: 'GET',
                    params: { id: '@id' },
                    transformResponse: function(data) {
                        try {
                            var responseObj = JSON.parse(data);
                            return responseObj;
                        } catch (e) {
                            return {};
                        }
                    }
                },

                create: {
                    method: 'POST',
                    transformResponse: function(data) {
                        try {
                            var responseObj = JSON.parse(data);
                            return responseObj;
                        } catch (e) {
                            return {};
                        }
                    }
                }
            });
        }
    ]);
