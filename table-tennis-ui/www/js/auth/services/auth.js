angular.module('tableTennis')
    .service('Auth', [
        '$resource',
        function ($resource) {
            "use strict";

            return $resource('http://localhost:9500/api/auth', {}, {
                login: {
                    method: 'POST',
                    url: 'http://localhost:9500/api/auth/login',
                    transformResponse: function (data) {
                        try {
                            var responseObj = JSON.parse(data);
                            return responseObj;
                        } catch (e) {
                        }
                    }
                },

                logout: {
                    method: 'GET',
                    url: 'http://localhost:9500/api/auth/logout',
                    transformResponse: function(data) {
                        var responseObj = JSON.parse(data);
                        return responseObj;
                    }
                }
            });
        }
    ]);
