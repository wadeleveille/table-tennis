angular.module('tableTennis')
    .controller('LogoutCtrl', [
        '$scope',
        'Auth',
        function($scope, Auth) {
            "use strict";

            Auth.logout();
        }
    ]);