angular.module('tableTennis')
    .controller('LoginCtrl', [
        '$scope',
        'Auth',
        'authService',
        function($scope, Auth, authService) {
            "use strict";

            $scope.user = {
                username: '',
                password: ''
            };

            $scope.login = function() {
                Auth.login($scope.user, function(data) {
                    authService.loginConfirmed();
                });
            };


        }
    ]);