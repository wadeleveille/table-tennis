angular.module('tableTennis')
    .directive('oppTimer', [
        '$rootScope',
        function ($rootScope) {
            'use strict';

            return {
                templateUrl: '../templates/timer.html',
                restrict: 'E',
                scope: {
                    currentTime: "=currentTime"
                },
                controller: [
                    '$scope', '$interval',
                    function ($scope, $interval) {
                        var TIMER_INTERVAL = 31;
                        var totalElapsedMs = 0;
                        var elapsedMs = 0;
                        var startTime;
                        var timerPromise;

                        if(!isNaN($scope.currentTime)) {
                            var now = new Date();
                            startTime = new Date();
                            now.setMilliseconds(now.getMilliseconds() + $scope.currentTime);
                            totalElapsedMs = now.getTime() - startTime.getTime();
                        }

                        $scope.startTimer = function(silent) {
                            if (!timerPromise) {
                                if(silent !== true) {
                                   $scope.$emit('timer-start');
                                }
                                startTime = new Date();
                                timerPromise = $interval(function() {
                                    var now = new Date();
                                    elapsedMs = now.getTime() - startTime.getTime();
                                }, TIMER_INTERVAL);
                            }
                        };

                        $scope.stopTimer = function(silent) {
                            if(timerPromise) {
                                $interval.cancel(timerPromise);
                                timerPromise = undefined;
                                totalElapsedMs += elapsedMs;
                                elapsedMs = 0;
                                if(silent !== true) {
                                   $scope.$emit('timer-stop', totalElapsedMs);
                                }
                            }
                        };

                        $scope.resetTimer = function(silent) {
                            startTime = new Date();
                            totalElapsedMs = elapsedMs = 0;
                            if(silent !== true) {
                               $scope.$emit('timer-reset', totalElapsedMs);
                            }
                        };

                        $scope.getElapsedMs = function() {
                            return totalElapsedMs + elapsedMs;
                        };

                        $scope.setTotalElapsedMs = function(ms) {
                            totalElapsedMs = ms;
                        };
                    }
                ],
                link: function(scope) {
                    scope.$on("start-timer", function(evt,currentTime,silent) {
                        scope.$apply(function () {
                            scope.setTotalElapsedMs(currentTime);
                            scope.startTimer(silent);
                        });
                    });
                    scope.$on("stop-timer", function(evt,currentTime,silent) {
                        scope.$apply(function () {
                            scope.stopTimer(silent);
                            scope.setTotalElapsedMs(currentTime);
                        });
                    });
                    scope.$on("reset-timer", function(evt,currentTime,silent) {
                        scope.$apply(function () {
                            scope.resetTimer(silent);
                        });
                    });
                }
            };
        }
    ]);
