angular.module('tableTennis')
    .directive('strokeCounter', [
        '$rootScope',
        function ($rootScope) {
            'use strict';

            return {
                templateUrl: '../templates/stroke-counter.html',
                restrict: 'E',
                scope: {
                    currentStrokes: "=currentStrokes"
                },
                controller: [
                    '$scope',
                    function ($scope) {
                        var currentStrokes = 0;

                        if(!isNaN($scope.currentStrokes)) {
                            currentStrokes = $scope.currentStrokes;
                        }

                        $scope.incrementStrokes = function(silent) {
                            $scope.setCurrentStrokes(currentStrokes + 1, silent);
                        };

                        $scope.decrementStrokes = function(silent) {
                            $scope.setCurrentStrokes(currentStrokes - 1, silent);
                        };

                        $scope.resetSrokes = function(silent) {
                            $scope.setCurrentStrokes(0, silent);
                        };

                        $scope.getCurrentStrokes = function() {
                            return currentStrokes;
                        };

                        $scope.setCurrentStrokes = function(strokes, silent) {
                            currentStrokes = strokes;
                            if(silent !== true) {
                               $scope.$emit('strokes-updated', strokes);
                            }
                        };
                    }
                ],
                link: function(scope) {
                    scope.$on("update-strokes", function(evt, currentStrokes, silent) {
                        scope.$apply(function () {
                            scope.setCurrentStrokes(currentStrokes, silent);
                        });
                    });
                }
            };
        }
    ]);
