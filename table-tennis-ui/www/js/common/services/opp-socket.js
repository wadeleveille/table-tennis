angular.module('tableTennis')
    .factory('OppSocket', [
        '$rootScope',
        function ($rootScope) {
            'use strict';

            var OppSocket = {

                mainSocket: io('http://localhost:9501'),
                gameSocket: null,

                openGameSocket: function(gameId) {
                    this.gameSocket = io('http://localhost:9501/' + gameId);
                },

                getMainSocket: function() {
                    return this.mainSocket;
                },

                getGameSocket: function() {
                    return this.gameSocket;
                },

                sendMessage: function(socket, messageName, messageBody) {
                    socket.emit(messageName, messageBody);
                },

                afterMessage: function (socket, messageName, callBack) {
                    socket.on(messageName, callBack);
                }
            };

            return OppSocket;
        }
    ]);
