angular.module('tableTennis')
    .controller('AppCtrl', [
        '$scope',
        'authService',
        '$ionicModal',
        'Auth',
        '$q',
        '$state',
        function($scope, authService, $ionicModal, Auth, $q, $state) {
            "use strict";

            $scope.$on('event:auth-loginRequired', function (e, rejection) {
                if($scope.initializingLogin !== true) {
                    $scope.initializingLogin = true;
                    if(!$scope.loginModal) {
                        $ionicModal.fromTemplateUrl('templates/auth/login.html', function(modal) {
                                $scope.loginModal = modal;
                                $scope.loginModal.show();
                                $scope.initializingLogin = false;
                            },
                            {
                                scope: $scope,
                                animation: 'slide-in-up',
                                focusFirstInput: true
                            }
                        );
                    }
                }
            });

            $scope.$on('event:auth-loginConfirmed', function() {
                $scope.username = null;
                $scope.password = null;
                $scope.loginModal.hide();
                $scope.loginModal.remove();
                $scope.loginModal = undefined;
            });

            //Be sure to cleanup the modal by removing it from the DOM
            $scope.$on('$destroy', function() {
                $scope.loginModal.remove();
            });

            $scope.logout = function() {
                Auth.logout();
            };

            $scope.navigateTo = function(state,params,options) {
                var deferred = $q.defer();
                options = options || {};
                options.reload = options.reload || true;
                $state.go(state,params,options).then(function(){
                    deferred.resolve();
                }, function(){
                    //unable to navigate to state
                    deferred.reject();
                });
                return deferred.promise;
            };
        }
    ]);