angular.module('tableTennis')
    .controller('LeagueListCtrl', [
        '$scope',
        'League',
        'OppSocket',
        '$state',
        function($scope, League, OppSocket, $state) {
            "use strict";

            $scope.leagues = League.all();

            $scope.league = {
                name: ''
            };

            $scope.createLeague = function() {
                $scope.league = League.create($scope.league, function() {
                    $state.go('league-detail', { "leagueId": $scope.league._id });
                });
            };
        }
    ]);

