angular.module('tableTennis')
    .controller('LeagueDetailCtrl', [
        '$scope',
        'User',
        'OppSocket',
        'League',
        '$stateParams',
        function($scope, User, OppSocket, League, $stateParams) {
            "use strict";

            $scope.league = League.get({id: $stateParams.leagueId});
        }
    ]);