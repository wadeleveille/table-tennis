angular.module('tableTennis')
    .service('League', [
        '$resource',
        function ($resource) {
            "use strict";

            return $resource('http://localhost:9500/api/league/:id', {}, {
                all: {
                    method: 'GET',
                    url: 'http://localhost:9500/api/league',
                    isArray: true,
                    transformResponse: function (data) {
                        try {
                            var responseObj = JSON.parse(data);
                            return responseObj;
                        } catch (e) {
                            return {};
                        }
                    }
                },

                get: {
                    method: 'GET',
                    params: { id: '@id' },
                    transformResponse: function(data) {
                        try {
                            var responseObj = JSON.parse(data);
                            return responseObj;
                        } catch (e) {
                            return {};
                        }
                    }
                },

                create: {
                    method: 'POST',
                    transformResponse: function(data) {
                        try {
                            var responseObj = JSON.parse(data);
                            return responseObj;
                        } catch (e) {
                            return {};
                        }
                    }
                }
            });
        }
    ]);
