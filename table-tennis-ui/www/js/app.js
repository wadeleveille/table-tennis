// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('tableTennis', [
  'ionic',
  'ngResource',
  'http-auth-interceptor'
])

.run(function($ionicPlatform, $rootScope, $ionicHistory) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $httpProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

    // setup an abstract state for the tabs directive
    // .state('tab', {
    //   url: "/tab",
    //   abstract: true,
    //   templateUrl: "templates/tabs.html"
    // })

    // Each tab has its own nav history stack:

    .state('leagues', {
      cache: false,
      url: '/leagues',
      templateUrl: 'templates/league/league-list.html',
      controller: 'LeagueListCtrl'
    })
    .state('league-detail', {
      cache: false,
      url: '/league/:leagueId',
      templateUrl: 'templates/league/league-detail.html',
      controller: 'LeagueDetailCtrl'
    })
    .state('users', {
      cache: false,
      url: '/users',
      templateUrl: 'templates/user/user-list.html',
      controller: 'UserListCtrl'
    })
    .state('user-detail', {
      cache: false,
      url: '/user/:userId',
      templateUrl: 'templates/user/user-detail.html',
      controller: 'UserDetailCtrl'
    })
    .state('games', {
      cache: false,
      url: '/games',
      templateUrl: 'templates/game/game-list.html',
      controller: 'GameListCtrl'
    })
    .state('game-detail', {
      cache: false,
      url: '/game/:gameId',
      templateUrl: 'templates/game/game-detail.html',
      controller: 'GameDetailCtrl'
    })
    .state('logout', {
      cache: false,
      url: '/logout',
      templateUrl: 'templates/auth/logout.html',
      controller: 'LogoutCtrl'
    });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/leagues');

  $httpProvider.defaults.withCredentials = true;
});

