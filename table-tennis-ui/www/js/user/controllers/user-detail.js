angular.module('tableTennis')
    .controller('UserDetailCtrl', [
        '$scope',
        '$stateParams',
        'User',
        function($scope, $stateParams, User) {
            "use strict";
            $scope.user = User.get({id: $stateParams.userId});
            $scope.currentTime = 50;
        }
    ]);