angular.module('tableTennis')
    .controller('UserListCtrl', [
        '$scope',
        'User',
        'OppSocket',
        function($scope, User, OppSocket) {
            "use strict";

            $scope.users = User.all();
        }
    ]);

