angular.module('tableTennis')
    .directive('userItem', [
        '$rootScope',
        function ($rootScope) {
            'use strict';

            return {
                templateUrl: '../templates/user-item.html',
                restrict: 'E',
                scope: {
                    user: "=user"
                },
                controller: [
                    '$scope',
                    'OppSocket',
                    function ($scope, OppSocket) {
                        $scope.gameUser = {
                            user: $scope.user,
                            currentTime: 0,
                            currentStrokes: 0
                        }
                        $scope.$on('timer-start', function() {
                            OppSocket.sendMessage(OppSocket.getGameSocket(),'user:timerStart',$scope.gameUser);
                        });
                        $scope.$on('timer-stop', function(evt, currentTime) {
                            $scope.gameUser.currentTime = currentTime;
                            OppSocket.sendMessage(OppSocket.getGameSocket(),'user:timerStop',$scope.gameUser);
                        });
                        $scope.$on('timer-reset', function(evt, currentTime) {
                            $scope.gameUser.currentTime = currentTime;
                            OppSocket.sendMessage(OppSocket.getGameSocket(),'user:timerReset',$scope.gameUser);
                        });
                        OppSocket.afterMessage(OppSocket.getGameSocket(),'user:timerStarted',function(data){
                            if(data.user._id === $scope.gameUser.user._id) {
                                $scope.gameUser.currentTime = data.currentTime;
                                $scope.$broadcast("start-timer",$scope.gameUser.currentTime,true);
                            }
                        });
                        OppSocket.afterMessage(OppSocket.getGameSocket(),'user:timerStopped',function(data){
                            if(data.user._id === $scope.gameUser.user._id) {
                                $scope.gameUser.currentTime = data.currentTime;
                                $scope.$broadcast("stop-timer",$scope.gameUser.currentTime,true);
                            }
                        });
                        OppSocket.afterMessage(OppSocket.getGameSocket(),'user:timerResetted',function(data){
                            if(data.user._id === $scope.gameUser.user._id) {
                                $scope.gameUser.currentTime = data.currentTime;
                                $scope.$broadcast("reset-timer",$scope.gameUser.currentTime,true);
                            }
                        });


                        $scope.$on('strokes-updated', function(evt, currentStrokes) {
                            $scope.gameUser.currentStrokes = currentStrokes;
                            OppSocket.sendMessage(OppSocket.getGameSocket(),'user:strokesUpdated',$scope.gameUser);
                        });
                        OppSocket.afterMessage(OppSocket.getGameSocket(),'user:strokesUpdated',function(data){
                            if(data.user._id === $scope.gameUser.user._id) {
                                $scope.gameUser.currentStrokes = data.currentStrokes;
                                $scope.$broadcast("update-strokes",$scope.gameUser.currentStrokes,true);
                            }
                        });
                    }
                ],
                link: function(scope) {

                }
            };
        }
    ]);
